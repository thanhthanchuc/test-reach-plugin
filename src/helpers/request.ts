import axios from 'axios';

export const request = axios.create({
  // Timeout 30p
  baseURL: 'https://ven04150.service-now.com',
  timeout: 30 * 60 * 1000,
  headers: {
    'Accept': 'application/json',
  }
});
