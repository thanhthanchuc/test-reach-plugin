export type APIErrorResponse = any

export interface IncidentEntity {
  parent: string;
  made_sla: string;
  caused_by: string;
  watch_list: string;
  upon_reject: string;
  sys_updated_on: string;
  child_incidents: string;
  hold_reason: string;
  task_effective_number: string;
  approval_history: string;
  number: string;
  resolved_by: ResolvedBy;
  sys_updated_by: string;
  opened_by: OpenedBy;
  user_input: string;
  sys_created_on: string;
  sys_domain: SysDomain;
  state: string;
  route_reason: string;
  sys_created_by: string;
  knowledge: string;
  order: string;
  calendar_stc: string;
  closed_at: string;
  cmdb_ci: CmdbCi;
  delivery_plan: string;
  contract: string;
  impact: string;
  active: string;
  work_notes_list: string;
  business_service: BusinessService;
  priority: string;
  sys_domain_path: string;
  rfc: string;
  time_worked: string;
  expected_start: string;
  opened_at: string;
  business_duration: string;
  group_list: string;
  work_end: string;
  caller_id: CallerId;
  reopened_time: string;
  resolved_at: string;
  approval_set: string;
  subcategory: string;
  work_notes: string;
  universal_request: string;
  short_description: string;
  close_code: string;
  correlation_display: string;
  delivery_task: string;
  work_start: string;
  assignment_group: AssignmentGroup;
  additional_assignee_list: string;
  business_stc: string;
  description: string;
  calendar_duration: string;
  close_notes: string;
  notify: string;
  service_offering: string;
  sys_class_name: string;
  closed_by: ClosedBy;
  follow_up: string;
  parent_incident: string;
  sys_id: string;
  contact_type: string;
  reopened_by: string;
  incident_state: string;
  urgency: string;
  problem_id: string;
  company: Company;
  reassignment_count: string;
  activity_due: string;
  assigned_to: AssignedTo;
  severity: string;
  comments: string;
  approval: string;
  sla_due: string;
  comments_and_work_notes: string;
  due_date: string;
  sys_mod_count: string;
  reopen_count: string;
  sys_tags: string;
  escalation: string;
  upon_approval: string;
  correlation_id: string;
  location: string;
  category: string;
}

export interface ResolvedBy {
  link: string;
  value: string;
}

export interface OpenedBy {
  link: string;
  value: string;
}

export interface SysDomain {
  link: string;
  value: string;
}

export interface CmdbCi {
  link: string;
  value: string;
}

export interface BusinessService {
  link: string;
  value: string;
}

export interface CallerId {
  link: string;
  value: string;
}

export interface AssignmentGroup {
  link: string;
  value: string;
}

export interface ClosedBy {
  link: string;
  value: string;
}

export interface Company {
  link: string;
  value: string;
}

export interface AssignedTo {
  link: string;
  value: string;
}

