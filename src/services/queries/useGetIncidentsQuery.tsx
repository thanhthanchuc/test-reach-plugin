import { useQuery, UseQueryOptions } from 'react-query';
import { request } from '../../helpers/request';
import { IncidentEndpoint } from '../endpoint';
import { APIErrorResponse, IncidentEntity } from '../types.interface';

export type GetIncidentsQueryResponse = {
  result: IncidentEntity[]
}
export type GetIncidentsQueryOptions = UseQueryOptions<GetIncidentsQueryFunc, APIErrorResponse, GetIncidentsQueryResponse>;
export type GetIncidentsQueryFunc = () => GetIncidentsQueryResponse;
export type GetIncidentsQueryParams = {
  sysparm_limit?: number;
}

export function useGetIncidentsQuery(params: GetIncidentsQueryParams, options?: GetIncidentsQueryOptions) {
  return useQuery<GetIncidentsQueryFunc, APIErrorResponse, GetIncidentsQueryResponse>({
    queryKey: ['useGetIncidentsQuery', params],
    queryFn: () => request.get(IncidentEndpoint.list, { params }).then(res => res.data),
    ...options
  });
}
